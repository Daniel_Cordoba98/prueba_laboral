<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= ('Acciones') ?></li>
        <li><?= $this->Html->link('Crear Usuario', ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="users index large-9 medium-8 columns content">
    <h3><?= ('Usuarios') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('rol_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('tipo_documento') ?></th>
                <th scope="col"><?= $this->Paginator->sort('documento') ?></th>
                <th scope="col"><?= $this->Paginator->sort('nombre') ?></th>
                <th scope="col"><?= $this->Paginator->sort('apellido') ?></th>
                <th scope="col"><?= $this->Paginator->sort('empleado') ?></th>
                <th scope="col"><?= $this->Paginator->sort('estado') ?></th>
                <th scope="col" class="actions"><?= ('Acciones') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($users as $user): ?>
            <tr>
                <td><?= $this->Number->format($user->id) ?></td>
                <td><?= h($user->rol['rol']) ?></td>
                <td><?= h($user->tipo_documento) ?></td>
                <td><?= h($user->documento) ?></td>
                <td><?= h($user->nombre) ?></td>
                <td><?= h($user->apellido) ?></td>
                <td><?php if($user->empleado): ?>
                        <p style="color:red;">NO</p>
                    <?php else: ?>
                        <p style="color:#01DF01;">SI</p>
                    <?php endif ?>
                </td>
                <td><?php if($user->estado): ?>
                        <p style="color:#01DF01;">ACTIVO</p>
                    <?php else: ?>
                        <p style="color:red;">INACTIVO</p>
                    <?php endif ?>
                </td>
                <td class="actions" style="display:unset;">
                    <?= $this->Html->link('Ver', ['action' => 'view', $user->id]) ?>
                    <?= $this->Html->link('Editar', ['action' => 'edit', $user->id]) ?>
                    <?= $this->Form->postLink('Eliminar', ['action' => 'delete', $user->id], ['confirm' => __('Esta seguro de eliminar al usuario {0}?', $user->nombre . " " . $user->apellido)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . ('Primero')) ?>
            <?= $this->Paginator->prev('< ' . ('Anterior')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(('Siguiente') . ' >') ?>
            <?= $this->Paginator->last(('Ultimo') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => ('Pagina {{page}} de {{pages}}, mostrando {{current}} registro(s) de {{count}} en total')]) ?></p>
    </div>
</div>
