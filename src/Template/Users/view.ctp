<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= ('Acciones') ?></li>
        <li><?= $this->Html->link('Editar', ['action' => 'edit', $user->id]) ?> </li>
        <li><?= $this->Form->postLink('Eliminar', ['action' => 'delete', $user->id], ['confirm' => __('Esta seguro de eliminar el usuario {0}?', $user->nombre . " " . $user->apellido)]) ?> </li>
        <li><?= $this->Html->link('Lista de Usuarios', ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link('Crear Usuario', ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="users view large-9 medium-8 columns content">
    <h3><?= 'Id Usuario : ' . h($user->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= ('Rol') ?></th>
            <td><?= h($user->rol['rol']) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= ('Tipo Documento') ?></th>
            <td><?= h($user->tipo_documento) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= ('Nombre') ?></th>
            <td><?= h($user->nombre) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= ('Apellido') ?></th>
            <td><?= h($user->apellido) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= ('Documento') ?></th>
            <td><?= $user->documento ?></td>
        </tr>
        <tr>
            <th scope="row"><?= ('Empleado') ?></th>
            <td><?= $user->empleado ? ('No') : ('Si'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= ('Estado') ?></th>
            <td><?= $user->estado ? ('Activo') : ('Inactivo'); ?></td>
        </tr>
    </table>
</div>
