<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= ('Acciones') ?></li>
        <li><?= $this->Html->link('Lista de Usuarios', ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="users form large-9 medium-8 columns content">
    <?= $this->Form->create($user) ?>
    <fieldset>
        <legend><?= ('Editar Usuario') ?></legend>
        <?php
            echo $this->Form->input('rol_id', [
                'type' => 'select',
                'options' => $roles,
                'label' => false,
                'empty' => 'Rol del Empleado'
            ]);

            echo $this->Form->input('tipo_documento', [
                'type' => 'select',
                'options' => [
                    'CC' => 'CC',
                    'TI' => 'TI',
                    'CE' => 'CE',
                ],
                'label' => false,
                'empty' => 'Tipo Documento'
            ]);
            echo $this->Form->input('documento');
            echo $this->Form->input('nombre');
            echo $this->Form->input('apellido');
            echo $this->Form->input('empleado', [
                'type' => 'radio',
                'options' => [
                    'SI',
                    'NO'
                ],
                'label' => 'Es empleado de la empresa?'
            ]);
            echo $this->Form->input('estado');
        ?>
    </fieldset>
    <?= $this->Form->button('Guardar') ?>
    <?= $this->Form->end() ?>
</div>
