<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class UsersTable extends Table
{

    public function initialize(array $config)
    {
        $this->hasOne('rol', [
            'className' => 'Roles',
            'bindingKey' => 'rol_id',
            'foreignKey' => 'id',
        ]);
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('rol_id')
            ->notEmpty('rol_id');

        $validator
            ->scalar('tipo_documento')
            ->maxLength('tipo_documento', 100)
            ->requirePresence('tipo_documento', 'create')
            ->notEmpty('tipo_documento');

        $validator
            ->integer('documento')
            ->requirePresence('documento', 'create')
            ->notEmpty('documento');

        $validator
            ->scalar('nombre')
            ->maxLength('nombre', 100)
            ->requirePresence('nombre', 'create')
            ->notEmpty('nombre');

        $validator
            ->scalar('apellido')
            ->maxLength('apellido', 100)
            ->requirePresence('apellido', 'create')
            ->notEmpty('apellido');

        $validator
            ->boolean('empleado')
            ->requirePresence('empleado', 'create')
            ->notEmpty('empleado');

        $validator
            ->boolean('estado')
            ->requirePresence('estado', 'create')
            ->notEmpty('estado');

        return $validator;
    }

    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['rol_id'], 'Rols'));

        return $rules;
    }
}
