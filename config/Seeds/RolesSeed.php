<?php
use Migrations\AbstractSeed;

/**
 * Roles seed.
 */
class RolesSeed extends AbstractSeed
{
    public function run()
    {
        $data = [
            [
                'id' => 1,
                'rol' => 'Administrador',
            ],
            [
                'id' => 2,
                'rol' => 'Usuario',
            ],
        ];

        $table = $this->table('roles');
        $table->insert($data)->save();
    }
}
