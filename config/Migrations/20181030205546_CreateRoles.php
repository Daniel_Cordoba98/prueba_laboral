<?php
use Migrations\AbstractMigration;

class CreateRoles extends AbstractMigration
{
    public function change()
    {
        $table = $this->table('roles');
        $table->addColumn('rol', 'string', [
            'default' => null,
            'limit' => 100,
            'null' => false,
        ]);
        $table->create();
    }
}
